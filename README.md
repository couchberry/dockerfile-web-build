# Docker image for building the Couchberry frontend
This container image is intended to be used only for building the Couchberry
frontend. It contains all the necessary dependencies and script that will build
frontend from sources located in <code>/home/user/sources/</code> inside container.
The result of the build will be placed into the <code>/home/user/build/</code>
directory inside the container.

Please make sure to execute the container with proper directories mounted into
the container by adding the following parameters:
```
-v <path_to_frontend_sources>:/home/user/sources:Z
```
and
```
-v <path_to_dir_for_final_build>:/home/user/build:Z
```

## Getting the pre-built image
To use the pre-built image from Docker Hub, just pull it:
```
docker pull couchberry/web-build
```

## Building the image
Just clone the git repository and run:
```
docker build -t couchberry/web-build .
```
This will build the container image from local Dockerfile.

## Running the image
To run the image execute:
```
docker run \
    -ti \
    --rm \
    -v <path_to_frontend_sources>:/home/user/sources:Z \
    -v <path_to_dir_for_final_build>:/home/user/build:Z \
    couchberry/web-build
```

To debug a failure, just change the entry point to some shell by adding
<code>--entrypoint=/bin/bash</code>

## Dockerfile Git repository
The main repository with Dockerfile for this image is located on [GitLab](https://gitlab.com/couchberry/dockerfile-web-build).

This repository is mirrored on [Bitbucket](https://bitbucket.org/couchberry/dockerfile-web-build),
because Docker Hub supports automated builds only from repositories on Github
or Bitbucket.