#!/bin/bash

# Script to run couchberry frontend build inside $SRCDIR and
# putting the results into $BUILDDIR

set -x

TMP_BUILDDIR="$HOMEDIR/tmp"

# check the direcotry with sources
if [ ! -d "$SRCDIR" ]; then
    echo "ERROR: \$SRCDIR='$SRCDIR' is not a directory" && exit 1
fi

# check the direcotry for putting the outcome of build
if [ ! -d "$BUILDDIR" ]; then
    echo "ERROR: \$BUILDDIR='$BUILDDIR' is not a directory" && exit 1
fi

mkdir "$TMP_BUILDDIR"
cp -r -T "$SRCDIR" "$TMP_BUILDDIR"
# make sure the directory nor the link is there
rm -f "$TMP_BUILDDIR/node_modules"
ln -s "../node_modules" "$TMP_BUILDDIR/node_modules"
# make sure the directory nor the link is there
rm -f "$TMP_BUILDDIR/bower_components"
ln -s "../bower_components" "$TMP_BUILDDIR/bower_components"

pushd "$TMP_BUILDDIR"
HOME="$HOMEDIR" grunt build || (echo "ERROR: Build FAILED!" && exit 1)
popd

if [ ! -d "$TMP_BUILDDIR/dist" ]; then
    echo "ERROR: No '$TMP_BUILDDIR/dist' directory!" && exit 1
else
    echo "Copying the '$TMP_BUILDDIR/dist' to '$BUILDDIR'"
    cp -r -T "$TMP_BUILDDIR/dist" "$BUILDDIR"
fi
